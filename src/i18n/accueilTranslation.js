const translations = {
    fr: {
        accueil: {
            titre: "Développeuse web et Intégratrice en formation",
            contenu: "En formation dans le domaine du développement web pour élargir mon horizon et faire de mon métier quelque chose de passionnant"
        },
    },
    en: {
        accueil: {
            titre: "Web developer and integrator in progress",
            contenu: "Currently in training in the web development to have more oppurtunities and have a passionate job"
    
        },
    },
};

export default translations;