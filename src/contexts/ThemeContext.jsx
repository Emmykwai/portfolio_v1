import React, { createContext } from "react";


export const ThemeContext = createContext();

class ThemeContextProvider extends React.Component {
    state = {
        isLightTheme: true,
        light: { syntax: '#E8EDDF', bg: '#6f7a5e'},
        dark:  { syntax: '#E8EDDF', bg: '#333533'}
    };

    render() { 
        return (
    <ThemeContext.Provider value={{...this.state}}>
        {this.props.children}
    </ThemeContext.Provider>
        );
    }
}
 
export default ThemeContextProvider;