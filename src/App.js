import {  BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Navbar from './components/Navbar';
import Accueil from './components/Accueil';
import Realisations from './components/Realisations';
import Footer from './components/Footer';
import Contact from "./components/Contact";
import Competences from "./components/Competences";
import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";
import ThemeContextProvider from "./contexts/ThemeContext";


function App() {
    return (
    <div className="App">
   <I18nextProvider i18n={i18n}>
   <ThemeContextProvider>
  <Router>
    <Navbar />
    <Switch>
      <Route path="/" exact ><Accueil /> </Route>
      <Route path="/realisations" exact><Realisations /> </Route>
      <Route path="/competences" exact><Competences /> </Route>
      <Route path="/contact" exact><Contact /> </Route>
    </Switch>
    <Footer />
  </Router> 
  </ ThemeContextProvider> 
  </I18nextProvider>
    </div>
  );
}

export default App; 
