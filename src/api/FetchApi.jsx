import {useEffect, useState} from "react";

function FetchProjets() {
    const [projets, setProjets] = useState([]);
    useEffect(() => {
        async function fetchProjets() {
            const token = "PSFBxDmegsaMmtxkLsYL";
            const url = `https://gitlab.com/api/v4/users/7564276/projects`;
            const headers = new Headers();
            headers.append("Authorization", `Bearer ${token}`);
            const response = await fetch(url, {
                headers: headers
            });
            const fetchedProjets = await response.json(response);
            setProjets(fetchedProjets);
        }

        fetchProjets();
    }, []);
    return (
        <div>
            {projets.map(objet => <li className="liste_api"><a href={objet.http_url_to_repo} key={objet.id}>{objet.name}</a></li>)}

        </div>
    );
}

export default FetchProjets;
