const Contact = () => {
    return (
        <div className="contact">
            <h2 className="accueil_titre col align-self-center">Formulaire de contact</h2>
            <form className="col align-self-center mx-auto">
                <div className="form-group">
                    <label for="nom">Nom :</label>
                    <input type="text" className="form-control" id="nom" aria-describedby="text"/>
                </div>
                 <div className="form-group">
                    <label for="email">Adresse courriel :</label>
                    <input type="email" className="form-control" id="email" aria-describedby="email"/>
                </div>
                <div className="form-group">
                    <label for="message">Message :</label>
                    <textarea className="form-control" id="message"></textarea>
                </div>
                <button type="submit" className="btn btn-block btn-danger">Submit</button>
            </form>
        </div>
    );
}

export default Contact;