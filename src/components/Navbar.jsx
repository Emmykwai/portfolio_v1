import { Link, withRouter } from "react-router-dom";
import { ThemeContext } from '../contexts/ThemeContext'
import React from 'react';


class Navbar extends React.Component{
static contextType = ThemeContext
render(){ 
    const { isLightTheme, light, dark} = this.context;
    const theme = isLightTheme ? light : dark;
    return (
        <header>
        <nav className="navbar justify-content-end" style={{background: theme.bg, color: theme.syntax }}>
           <Link to="/" style={{color: theme.syntax}}>Accueil</Link>        
           <Link to="/competences" style={{color: theme.syntax}}>Compétences</Link>
           <Link to="/realisations" style={{color: theme.syntax}}>Réalisations</Link>
           <Link to="/contact" style={{color: theme.syntax}} className="last_a">Contact</Link>       
        </nav> 
        </header>  
    );
}
}
 
export default withRouter(Navbar)
;