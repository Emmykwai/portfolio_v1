import { useTranslation } from "react-i18next";;

const Accueil = () => {
  
    const { t } = useTranslation('accueil');
 
    return ( 
        <div className="accueil" >
            <h1 className="accueil_titre">{t("titre")}</h1>
            <p className="pAccueil mx-auto">{t("contenu")}</p>
       
        </div>
     );
}
 
export default Accueil; 