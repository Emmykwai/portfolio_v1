

const Footer = () => {
    return (
        <div className="row d-flex fixed-bottom">
            <div className="col-sm icones">
                <p> Made by me &copy; 2021 </p>
            </div>
            <div className="col-sm icones">
                <a href="http://www.linkedin.com/in/emeline-leclere-04aa87216/"><i className="fab fa-linkedin-in"></i></a>
                <a href="/contact"><i className="fas fa-envelope"></i></a>
            </div>
        </div>
    );
}

export default Footer;